use callee::Callee;
fn main() {
    // Initialising the callee
    let callee = Callee::init().unwrap();
    // Print the comm
    println!("Callee comm: {}", callee.comm());
    // Print the cmdline
    println!("Callee cmdline: {}", callee.cmdline());
}
