use callee::Callee;

fn main() {
    // Initialising of the callee
    let callee = Callee::init().unwrap();
    let serialized = serde_json::to_string(&callee).unwrap();
    // Print the json representation of the object
    println!("{serialized}");
}
