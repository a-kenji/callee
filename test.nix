{
  pkgs,
  example-callee,
  example-callee-json,
}: let
  sharedModule = {
    virtualisation.graphics = false;
  };
in
  pkgs.nixosTest {
    name = "example-callee";
    nodes = {
      client = {
        imports = [sharedModule];
        environment.systemPackages = [example-callee example-callee-json];
      };
    };
    testScript = ''
      import json

      start_all()

      callee = client.succeed("${example-callee}/bin/example-callee")
      print(callee)
      assert callee == "Callee comm: bash\nCallee cmdline: bash --norc /dev/hvc0\n"

      expected = [
        "bash --norc /dev/hvc0", "bash"
      ]

      actual = json.loads(
          client.succeed(
       "${example-callee-json}/bin/example-callee-json")
      )

      assert actual == expected
    '';
  }
