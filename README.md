# callee

[![Built with Nix](https://img.shields.io/static/v1?label=built%20with&message=nix&color=5277C3&logo=nixos&style=flat-square&logoColor=ffffff)](https://builtwithnix.org)
[![Crates](https://img.shields.io/crates/v/callee?style=flat-square)](https://crates.io/crates/callee)
[![Documentation](https://img.shields.io/badge/callee-documentation-fc0060?style=flat-square)](https://docs.rs/callee)

## Description

The `callee` library provides a simple way to retrieve information about the calling process in Rust.

Features:
- Retrieve the command line arguments of the calling process.
- Retrieve the binary name of the calling process.
- Access process information such as the comm attribute.


## Usage:

```
callee = "0.1.0"
```

or use `cargo add`:

```
cargo add callee
```

## Example:
```
    use callee::Callee;
    let callee = Callee::init().unwrap();
    println!("Comm: {}", callee.comm());
    println!("Info: {}", callee.info());
```


## Limitations

The library is currently tested on Linux only, and its compatibility with Darwin or Windows systems is not guaranteed. Contributions to make it cross-platform are welcome.

If you have different needs or find improvements, I am open to contributions. Please see the contributing section for more details.

## Changes
[Changelog](./CHANGELOG.md)


## Contributing
[How to contribute.](./docs/CONTRIBUTING.md)

## License
MIT
